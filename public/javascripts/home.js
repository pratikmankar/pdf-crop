var home = {
    init: function () {
        this.bind_events();
    },
    bind_events: function () {
        var self = this;
        $("#cropScreen").click(function () {
            if ($("#modelName").val() == "") {
                alert("Model name cannot be empty!")
                $("#modelName").focus()
                $("#modelName").addClass("is-invalid")
                return false;
            } else if ($("#pdfFile").val() == "") {
                alert("PDF file cannot be empty!")
                $("#pdfFile").addClass("is-invalid")
                return false;
            }
            location.href = self.base_url + '/editor?pdf=' + $("#cropScreen").attr("data-file");
        });
        $("#modelName").on("input", function () {
            if ($(this).val() !== "") {
                $("#modelName").removeClass("is-invalid")
            } else {
                $("#modelName").addClass("is-invalid")
            }
        });
        $("#train").click(function () {
            self.train();
        })
    },
    train: function (values, event) {
        var self = this;

        if ($("#modelName").val() == "") {
            alert("Model name cannot be empty!")
            $("#modelName").focus()
            $("#modelName").addClass("is-invalid")
            return false;
        } else if ($("#pdfFile").val() == "") {
            alert("PDF file cannot be empty!")
            $("#pdfFile").addClass("is-invalid")
            return false;
        }
        $("#modelName").removeClass("is-invalid")
        $("#modelName").addClass("is-invalid")
        
        $(".loader").show();
        var fileData = $("#pdfFileInput input[type='file']")[0].files[0];
        var form_data = new FormData();
        form_data.append('file', fileData);
        form_data.append('file_name', fileData.name);
        form_data.append('model_name', $("#modelName").val());
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: self.api_url +"/train",
            data: form_data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            headers: {
                model_name: modelName
            },
            success: function (data, xhr, status, textStatus) {
                if (data.title == "success") {
                    location.href = self.base_url + "/predict"
                } else {
                    alert(data.message);
                }
                $(".loader").hide()
            },
            error: function () {
                $(".loader").hide()
                alert("Something went wrong!");
            }
        });

    },
    uploadPdf: function (values, event) {
        var self = this;
        if ($("#modelName").val() == "") {
            alert("Model name cannot be empty!")
            $("#modelName").focus()
            $("#modelName").addClass("is-invalid");
            return false;
        }
        $("#modelName").removeClass("is-invalid");
        $("#pdfFile").removeClass("is-invalid");
        var uploadfile = $("#pdfFileInput input[type='file']")[0].files[0];
        var form_data = new FormData();
        form_data.append('uploadfile', uploadfile);
        form_data.append('file_name', uploadfile.name);
        $("#pdfFile").siblings("label").text(uploadfile.name);
        $("#cropScreen").attr("disabled", true);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: self.base_url + "/api/uploadPdf",
            data: form_data,
            processData: false,
            contentType: false,
            cache: false,
            headers: {
                folder_name: $("#modelName").val()
            },
            success: function (data, xhr, status, textStatus) {
                $("#cropScreen").attr("disabled", false);
                $("#cropScreen").attr("data-file", data.filePath);
                alert("File Uploaded");
            },
            error: function () {
                alert("Something went wrong!");
            }
        });
    }
};