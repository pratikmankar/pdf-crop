const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const fs = require('fs');
const multer = require('multer');
const path = require('path');
const PDFController = require('../controllers/PDF.controller');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        var directory = path.join(__basedir, '/public/uploads/' + req.headers.folder_name);
        console.log(directory)
        if (fs.existsSync(directory)) {
            console.log("Directory exists.")
            cb(null, directory)
        } else {
            console.log("Directory does not exist.")
            fs.mkdir(directory, { recursive: true }, function(err) {
                if (err) {
                  console.log(err)
                } else {
                  console.log("New directory successfully created.")
                  cb(null, directory)
                }
            })
        }
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
    }
});

const upload = multer({
    storage: storage
});


router.post('/cropPdf', PDFController.cropPdf);
router.post('/uploadPdf', upload.single("uploadfile"), PDFController.uploadPdf);

module.exports = router;