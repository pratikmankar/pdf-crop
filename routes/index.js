var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {});
});
router.get('/editor', function (req, res, next) {
  res.render('editor', {
    fileName: encodeURIComponent(req.query.pdf),
    trainMode: req.query.trainMode
  });
});
router.get('/predict', function (req, res, next) {
  res.render('predict', {});
});

module.exports = router;