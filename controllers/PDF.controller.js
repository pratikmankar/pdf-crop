const {
    exec
} = require('child_process');

exports.cropPdf = async (req, res, next) => {

    const inputPath = 'public/' + req.body.input;
    const outputFileName = inputPath.split("/")[inputPath.split("/").length - 1].split(".").join("_output.");
    const outputPath = 'public/uploads/' + inputPath.split("/")[2] + '/' + outputFileName;

    console.log("Input File Path: ", inputPath);
    console.log("Output File Path: ", outputPath);

    const giveWriteAccess = async () => {
        return new Promise(async (callback) => {
            exec(`chmod -R 777 public`, function (error, stdout, stderr) {
                if (error) {
                    return res.json({
                        title: "error",
                        message: "Failed to give write access to /public directory",
                        data: error,
                        stdout,
                        stderr
                    })
                } else {
                    console.log("Given write access to /public directory");
                    callback("success")
                }
            });
        })
    }

    const execTwo = await giveWriteAccess();

    await Promise.all([execTwo]).then(values => {
        console.log("Cropping PDF...");
        setTimeout(function () {
            const cmd = `pdfcrop --margins '${req.body.points}' ${inputPath} ${outputPath}`;
            exec(cmd, function (error, stdout, stderr) {
                if (error) {
                    console.log("Something went wrong while croppping PDF");
                    console.log("ERROR Log: ", error);
                    return res.json({
                        title: "error",
                        croppedFile: null,
                        data: error,
                        stdout,
                        stderr
                    })
                } else {
                    console.log("PDF Cropped");
                    console.log("File Path: ", outputPath);
                    return res.json({
                        title: "success",
                        croppedFile: outputPath,
                        stdout,
                        stderr
                    })
                }
            });
        }, 5000)
    })

}


exports.uploadPdf = async (req, res, next) => {
    return res.json({
        title: "success",
        fileName: req.file.filename,
        filePath: `uploads/${req.headers.folder_name}/${req.file.filename}`
    })
}